package assignment6;
public class Day {
	
	/**
	 * @author cjp2790
	 * 11/15/2014
	 * This is the main class file for this assignment.
	 */
	 
	
	/**
	 * @param day1 Create day1 string
	 * @param dayno Create dayno int
	 */
	private String day1;
	private int dayno;
	String[] dayarray = { "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", };
	public Day(){
		day1 = dayarray[0];
		dayno = 1;
	}

	public String toString() {
		return "\n The day is " + day1 + ".";
	}
	public String getDay() {
		return day1;
	}

	public void setDay(int x) {
		day1 = dayarray[x - 1];
		dayno = x;
	}

	public void nextday() {
		if (dayno <= 6) {
			dayno = dayno + 1;
			setDay(dayno);
			getDay();
		} else {
			setDay(1);
			getDay();
		}
	}

	public void prevday() {
		if (dayno >= 2) {
			dayno = dayno - 1;
			setDay(dayno);
			getDay();
		} else {
			setDay(7);
			getDay();
		}
	}
	
	public void addday(int x) {
		int temp = dayno;
		for(int y = (temp-1);y<=(x+(temp-2));y++) {
		nextday();
		}
		toString();
	}
}