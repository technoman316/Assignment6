package assignment6;

public class DayDriver {

	public static void main(String[] args) {
		/**
		 * @author cjp2790
		 * 11/15/2014
		 * This is the driver to accompany Day.java
		 */
		
		/**
		 * @param test Create new Day
		 */
		Day test = new Day();
		System.out.print(test.toString());
		test.setDay(2);
		System.out.print(test.toString());
		test.nextday();
		test.prevday();
		test.addday(1);
		System.out.print(test.toString());
	}

}
